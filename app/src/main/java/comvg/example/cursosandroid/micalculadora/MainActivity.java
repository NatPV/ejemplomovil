package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResult;
    private Button btnSumar, btnRestar, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniComponents();
        setEventos();
    }

    public void iniComponents() {
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        txtResult = findViewById(R.id.txtResult);

        btnSumar = findViewById(R.id.btnSuma);
        btnRestar = findViewById(R.id.btnResta);
        btnMulti = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDivi);

        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
    }

    public void setEventos() {
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnCerrar.getId())
            finish();
        else if (view.getId() == btnLimpiar.getId()) {
            txtResult.setText("");
            txtNum2.setText("");
            txtNum1.setText("");
        } else {
            if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")) {
                Toast.makeText(this, "Ingrese valores", Toast.LENGTH_SHORT).show();
            } else {
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                switch (view.getId()) {
                    case R.id.btnSuma:
                        txtResult.setText(String.valueOf(op.suma()));
                        break;

                    case R.id.btnResta:
                        txtResult.setText(String.valueOf(op.resta()));
                        break;

                    case R.id.btnMult:
                        txtResult.setText(String.valueOf(op.mult()));
                        break;

                    case R.id.btnDivi:
                        txtResult.setText(String.valueOf(op.div()));
                        break;
                }
            }
        }
    }
}
